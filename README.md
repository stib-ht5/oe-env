## **The HanoverOS 2.x Build System** 

The HanoverOS 2.x is a Linux based build system derived from a tree provided by Texas Instruments (TI). The TI tree is a direct fork of the **Arago** tree, which in turn is a direct fork of the **Ångström** distribution. The Ångström distribution is a direct fork of the **OpenEmbedded** tree. This forking flow is shown below:



```
			HanoverOS 2.x <---- Texas Instruments <---- Arago <---- Ångström <---- OpenEmbedded 
```



This document explains how to setup, build and deploy hanover stib images, based on the Hanover Operating System version 2.x (aka HanoverOS 2.x). This tree is used for building Hanover Application Systems targetting **TI dm814x** (HT5) based boards. The workspace is maintained via **Google Repo**, and is built inside a **docker** container using wrapper scripts that call **bitbake**.



Note: Unless specified, all commands below should be executed as a **normal user**, not as **root user**.



## **Setting up the Build Server (host machine)**

### **System Requirements Recommendations**

```
* Operating System: Ubuntu 18.04
* System RAM: minimum 32 GB 
* System Storage: minimum 1 TB
* Processors: Quad Core (Xeon Silver 4114 or better) 
```

### **Install git and repo** 

Install git and repo:

```
 $ sudo apt update
 $ sudo apt-get install git
```
```
 $ sudo apt-get install python
 $ sudo -i
 $ curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo
 $ chmod +x /usr/local/bin/repo
 $ exit
```

### **Configure git with personal details** 

Firstly, check if git is configured with your personal details:

```
 $ git config user.name 
 $ git config user.email 
```

If not, set them accordingly:
```
 $ git config --global user.name "Your Name"
 $ git config --global user.email "you@hanoverdisplays.com"
```


### **Install docker** 

```
 $ sudo -i
 $ curl -L https://get.docker.io/ | sudo bash
 $ exit
```


### **Add user to docker group**
```
 $ sudo usermod -a -G docker cchilumbu
``` 

The user in examples given in this document is cchilumbu, but this should be replaced with actual username. To apply the new group membership, log out of the server and back in, or type the following:

```
 $ su - cchilumbu
```

You will be prompted to enter your user's password to continue. Confirm that your user is now added to the docker group by typing:

```
 $ id -nG
 cchilumbu sudo docker
```

The above shows that user **cchilumbu** is a member of the **docker** group.


### **Make sure docker daemon is running**
```
 $ sudo -i
 $ systemctl stop docker
 $ CONFIGURATION_FILE=$(sudo systemctl show --property=FragmentPath docker | cut -f2 -d=)
 $ cp $CONFIGURATION_FILE /etc/systemd/system/docker.service
 $ perl -pi -e 's/^(ExecStart=.+)$/$1 -s overlay/' /etc/systemd/system/docker.service
 $ systemctl daemon-reload
 $ curl -sSL https://get.docker.com/ | sh
 $ systemctl start docker
 $ exit
```


### **Add ssh public key to git servers** 

In order to connect to the git servers, **ssh public keys** need to be authorised on those servers listed in the manifest file.

By default **ssh** will attempt to use $HOME/.ssh/id_rsa to connect to the servers, and the corresponding public key is most probably at $HOME/.ssh/id_rsa.pub. The public key is a single line of text with three space delimited fields (type, encoded-content, comment) e.g.:


```
 ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDdfhBJW8tgNWjlvSo2BD/S5JCYXQ1C5n+trk1GVB/vyV+JDoJbnNSiJnZrfEwNmD5tXpAEg0T3z7r79N0IFe5g5jAcXmZ9i0QWyFqN0n8lC1N7nyflvZSeMamfkLb5n2b82Sfhxa/oL271Ietn5JNmX0UwN5rgKTwiImqp5tybX8vjD6GPbcVP3OyTN+DJG/jcZEfBb1L6cPc/0YlzzcnQBXIGBJBlSYyPBTZHu7byCnyVJhABKGxIC2GGUMj5IQi92QN3mqgJ781oFkP5Np2ZnNUso+KbToY+PcLPrXKRNq4T95Gd3RPFn+fDKmr9D1JlLizX22vPoQLvHFsbBaaR stib@corbyn
```


New ssh keys maybe created by running the following on the build server:
```
   $ ssh-keygen
```


Then add this public ssh key to your **user profile** on Bitbucket (navigate to Bitbucket security):
```
https://bitbucket.org/account/user/stib-ht5/ssh-keys/
``` 

The above key should not be confused with the deployment key, and such, care should be taken to add the ssh key in the correct place in Bitbucket.


## **The HanoverOS 2.x Workspace**
The workspace is the directory in which all assets needed to build the target image are found.


### **Naming the Workspace**
The workspace (aka base directory) is used for building one particular variant of the Operating System.

Please follow guidelines below in naming the workspace:
```
* It is recommended to prefix all workspaces with oe-.
* It is recommended to include the application name within the workspace name e.g. stib.
* It is also recommended to include the machine name within the workspace name e.g. ht5.
* Sometimes a differentiator might be useful, if for example, you would like to build the same thing using different branches e.g. sdk or next.
```

A suitable workspace name for this project might be **oe-stib-ht5-sdk** or **oe-stib-ht5-next**. 


### **Creating the Workspace**
Once authenticated access is granted on the build server, a workspace can be created, e.g. (in this example, workspaces are created under **projects** directory for a user called cchilumbu):

```
 $ sudo mkdir ~/projects
 $ sudo mkdir ~/projects/oe-stib-ht5-sdk
 $ sudo chown -R cchilumbu:cchilumbu /home/cchilumbu

```

Please remember to replace cchilumbu above with your own user name.


### **Initialising the Workspace**

To initialise a workspace, use **repo** command with the URL of the manifests repository as an argument, as shown in the examples below:


```
 $ cd ~/projects/oe-stib-ht5-sdk
 $ repo init -u git@bitbucket.org:stib-ht5/oe-env
```

This tells **repo tool** to create a workspace using a manifest called **default.xml** from the **master** branch of **https://stib-ht5@bitbucket.org/stib-ht5/oe-env**


And this point, the workspace can be populated using repo sync.


To specify a different manifest file, use **-m** flag, e.g.:
```
 $ cd ~/projects/oe-stib-ht5-sdk
 $ repo init -u git@bitbucket.org:stib-ht5/oe-env -m baseline.xml
```



To specify a different branch or a tag, use **-b** flag, e.g.:
```
 $ cd ~/projects/oe-stib-ht5-sdk
 $ repo init -u git@bitbucket.org:stib-ht5/oe-env -b refs/tags/2.2.34+stibsdk.1
```

After completing workspace initialisation, a **.repo** directory will be present in top level base directory.


A snapshot of the workspace after repo init is shown below:
```
 $ ls -la ~/projects/oe-stib-ht5-sdk
 total 12
 drwxrwxr-x  3 cchilumbu cchilumbu 4096 Jan 29 08:50 .
 drwxrwxrwx 13 cchilumbu cchilumbu 4096 Jan 29 08:50 ..
 drwxrwxr-x  5 cchilumbu cchilumbu 4096 Jan 29 08:50 .repo
```


By convention, please keep branches in **default.xml**, and tags in **baseline.xml**.


The workspace can be reinitialised by specifying only the part that is required to be changed using repo tool flags e.g. **( -u, -b, -m)**.


### **Populating the Workspace (Fetch Sources)**
Next, synchronise the workpace with repositories specified in the manifest:

```
 $ cd ~/projects/oe-stib-ht5-sdk
 $ repo sync -j4
```

When successful, the workspace is populated with the selected directories from the manifest. 

The same process has to be done every time that a repository changes (calling repo init and then repo sync). 


In this SDK, the **downloads** directory (that maintains fetched package tarballs), and the **TIS** release are both part of the manifest, so repo sync will populate them into the workspace:

A snapshot of the workspace after repo sync as shown below:

```
 $ ls -la ~/projects/oe-stib-ht5-sd
 total 52
 drwxrwxr-x 13 cchilumbu cchilumbu 4096 Jan 29 09:02 .
 drwxrwxrwx 13 cchilumbu cchilumbu 4096 Jan 29 08:50 ..
 lrwxrwxrwx  1 cchilumbu cchilumbu   34 Jan 29 09:02 all.sh -> hanover-products/stib/build_all.sh
 drwxrwxr-x  7 cchilumbu cchilumbu 4096 Jan 29 09:02 arago
 drwxrwxr-x  9 cchilumbu cchilumbu 4096 Jan 29 09:02 arago-bitbake
 drwxrwxr-x 12 cchilumbu cchilumbu 4096 Jan 29 09:02 arago-oe-dev
 lrwxrwxrwx  1 cchilumbu cchilumbu   29 Jan 29 09:02 bb.sh -> hanover-oe-utils/oe-env/bb.sh
 lrwxrwxrwx  1 cchilumbu cchilumbu   32 Jan 29 09:02 clean.sh -> hanover-oe-utils/oe-env/clean.sh
 drwxrwxr-x  3 cchilumbu cchilumbu 4096 Jan 29 09:02 docker
 lrwxrwxrwx  1 cchilumbu cchilumbu   27 Jan 29 09:02 downloads -> hanover-downloads/downloads
 drwxrwxr-x  5 cchilumbu cchilumbu 4096 Jan 29 09:02 hanover-apps
 drwxrwxr-x  4 cchilumbu cchilumbu 4096 Jan 29 09:02 hanover-downloads
 drwxrwxr-x  5 cchilumbu cchilumbu 4096 Jan 29 09:02 hanover-oe-utils
 drwxrwxr-x  3 cchilumbu cchilumbu 4096 Jan 29 09:02 hanover-products
 drwxrwxr-x  5 cchilumbu cchilumbu 4096 Jan 29 09:02 hanover-system
 drwxrwxr-x  4 cchilumbu cchilumbu 4096 Jan 29 09:02 hanover-tis
 drwxrwxr-x  7 cchilumbu cchilumbu 4096 Jan 29 09:02 .repo
 lrwxrwxrwx  1 cchilumbu cchilumbu   13 Jan 29 09:02 run.sh -> docker/run.sh
 lrwxrwxrwx  1 cchilumbu cchilumbu   22 Jan 29 09:02 SW-HT5-TIS -> hanover-tis/SW-HT5-TIS
 lrwxrwxrwx  1 cchilumbu cchilumbu   32 Jan 29 09:02 times.sh -> hanover-oe-utils/oe-env/times.sh
```


## **Building the Target Image**

To build an image for the HT5 target:

```
 $ cd ~/projects/oe-stib-ht5-sdk
 $ ./all.sh -r hanover-stib-image
```

This will make all the packages required for the HT5 image. The components generated are shown below: 

```
 $ ls -la ~/projects/oe-stib-ht5-sdk/arago-tmp/stib/rel/deploy/eglibc/images/dm814x-ht5
 total 128904
 drwxr-xr-x 5 cchilumbu cchilumbu     4096 Jan 21 14:53 .
 drwxr-xr-x 3 cchilumbu cchilumbu     4096 Jan 21 11:44 ..
 -rw-rw-r-- 1 cchilumbu cchilumbu 19660455 Jan 21 13:23 data-01.stib.r04-rc61.zip
 lrwxrwxrwx 1 cchilumbu cchilumbu       25 Jan 21 13:23 data-01.zip -> data-01.stib.r04-rc61.zip
 -rw-rw-r-- 1 cchilumbu cchilumbu 19660475 Jan 21 13:23 data-02.stib.r04-rc61.zip
 lrwxrwxrwx 1 cchilumbu cchilumbu       25 Jan 21 13:23 data-02.zip -> data-02.stib.r04-rc61.zip
 -rw-r--r-- 1 cchilumbu cchilumbu      375 Jan 21 11:44 hanover-stib-version
 -rw-r--r-- 1 cchilumbu cchilumbu      332 Jan 21 11:44 hanover-version
 lrwxrwxrwx 1 cchilumbu cchilumbu       53 Jan 21 13:40 kmodules.tgz -> modules-2.6.37.6-HDL-HT5-20180731-r1.1-dm814x-ht5.tgz
 -rw-rw-r-- 1 cchilumbu cchilumbu   484320 Jan 21 13:40 modules-2.6.37.6-HDL-HT5-20180731-r1.1-dm814x-ht5.tgz
 drwxr-xr-x 2 cchilumbu cchilumbu     4096 Jan 21 14:15 scripts
 lrwxrwxrwx 1 cchilumbu cchilumbu       63 Jan 21 14:53 stib-image.r04-rc61-dm814x-ht5.tar.gz -> stib-image.r04-rc61-eglibc-ipk-2011.09-dm814x-ht5.rootfs.tar.gz
 -rw-rw-r-- 1 cchilumbu cchilumbu 89489863 Jan 21 14:53 stib-image.r04-rc61-eglibc-ipk-2011.09-dm814x-ht5.rootfs.tar.gz
 drwxrwxr-x 2 cchilumbu cchilumbu     4096 Jan 21 14:41 stib-image.r04-rc61-eglibc-ipk-2011.09-dm814x-ht5-testlab
 lrwxrwxrwx 1 cchilumbu cchilumbu       31 Jan 21 13:17 u-boot -> u-boot-2010.06_HDL_HT5_20160911
 drwxr-xr-x 2 cchilumbu cchilumbu     4096 Jan 21 13:17 u-boot-2010.06_HDL_HT5_20160911
 -rw-r--r-- 1 cchilumbu cchilumbu  2659440 Jan 21 13:40 uImage-2.6.37.6-HDL-HT5-20180731-r1.1-dm814x-ht5.bin
 lrwxrwxrwx 1 cchilumbu cchilumbu       52 Jan 21 13:40 uImage.bin -> uImage-2.6.37.6-HDL-HT5-20180731-r1.1-dm814x-ht5.bin
 lrwxrwxrwx 1 cchilumbu cchilumbu       52 Jan 21 13:40 uImage-dm814x-ht5.bin -> uImage-2.6.37.6-HDL-HT5-20180731-r1.1-dm814x-ht5.bin
```


All the elements needed to assemble a full release are now available. However, before a full release image can be built, the file release.history should be updated with a summary of the latest changes. The full path to this file is:  ~/projects/oe-stib-ht5/hanover-products/stib/release.history.


The full release process consists of assembling the above elements into a defined directory structure which can be easily processed by the final deployment step. 


Next, build the release image:
```
 $ cd ~/projects/oe-stib-ht5-sdk/arago-tmp/stib/rel/deploy/eglibc/images/dm814x-ht5 
 $ ./scripts/release.sh
```

The resulting components will be under a directory with a name similar to *r04-rc61*, and a snapshot of this directory is shown below:

```
 $ ls -al ~/projects/oe-stib-ht5-sdk/arago-tmp/stib/rel/deploy/eglibc/images/dm814x-ht5/release/SW-HT5-STIB/r04-rc61
 total 107104
 drwxrwxr-x 4 cchilumbu cchilumbu     4096 Jan 21 15:39 .
 drwxrwxr-x 3 cchilumbu cchilumbu     4096 Jan 21 15:39 ..
 -rw-rw-r-- 1 cchilumbu cchilumbu 19660475 Jan 21 15:39 data-02.zip
 drwxrwxr-x 6 cchilumbu cchilumbu     4096 Jan 21 14:51 ipk
 -rw-r--r-- 1 cchilumbu cchilumbu   194862 Jan 21 15:39 opkg_status.txt
 drwxr-xr-x 2 cchilumbu cchilumbu     4096 Jan 21 14:15 scripts
 -rw-rw-r-- 1 cchilumbu cchilumbu 89489863 Jan 21 15:39 SW-HT5-STIB-rootfs.tar.gz
 -rw-rw-r-- 1 cchilumbu cchilumbu   301795 Jan 21 15:39 SW-HT5-STIB.txt
 -rw-r--r-- 1 cchilumbu cchilumbu      375 Jan 21 15:39 versions.txt
```


**Note The next steps need to be carried out on a system where the User is on the sudoers list and has access to external devices**


## **Making SD card Image**

If not already installed on the build server, please install kpartx (a tool for mounting partitions within an image):

```
 $ sudo apt-get install kpartx
```

For HT5 target, additional software is needed to make the SD card image. This sofware, called **Test Interface Software** (or TIS for short), and is already part of the workspace in this SDK release. 


Make an SD card image using TIS:
```
 $ cd ~/projects/oe-stib-ht5-sdk/arago-tmp/stib/rel/deploy/eglibc/images/dm814x-ht5/release/SW-HT5-STIB/r04-rc61
 $ sudo ~/projects/oe-stib-ht5-sdk/SW-HT5-TIS/0.2.7.1/scripts/mksdimage.sh .
```


A snapshot of what is generated is shown below:

```
 $ ls -la ~/projects/oe-stib-ht5-sdk/arago-tmp/stib/rel/deploy/eglibc/images/dm814x-ht5/release/SW-HT5-STIB/r04-rc61
 total 516268
 drwxrwxr-x 4 cchilumbu cchilumbu       4096 Jan 21 17:05 .
 drwxrwxr-x 3 cchilumbu cchilumbu       4096 Jan 21 15:39 ..
 -rw-r--r-- 1 root      root           69148 Jan 21 17:05 boot.bin
 -rw-rw-r-- 1 cchilumbu cchilumbu   19660475 Jan 21 15:39 data-02.zip
 drwxrwxr-x 6 cchilumbu cchilumbu       4096 Jan 21 14:51 ipk
 -rw-r--r-- 1 cchilumbu cchilumbu     194862 Jan 21 15:39 opkg_status.txt
 drwxr-xr-x 2 cchilumbu cchilumbu       4096 Jan 21 14:15 scripts
 -rw-r--r-- 1 cchilumbu cchilumbu 1992294400 Jan 21 17:05 SW-HT5-STIB-02.1900.ima
 -rw-rw-r-- 1 cchilumbu cchilumbu   89489863 Jan 21 15:39 SW-HT5-STIB-rootfs.tar.gz
 -rw-rw-r-- 1 cchilumbu cchilumbu     301795 Jan 21 15:39 SW-HT5-STIB.txt
 -rw-r--r-- 1 cchilumbu cchilumbu        375 Jan 21 15:39 versions.txt
```

The image of interest above is **SW-HT5-STIB-02.1900.ima**.


## **Writing Image to SD Card** 

The image created above now needs to be written to SD card. **Be careful not to copy the image onto one of your local disks**, so find out precisely what device the SD card is allocated to.

```
 $ sudo dd if=SW-HT5-STIB-02.1900.ima of=/dev/sdc bs=1M status=progress
 $ sync
```

Once writing to SD card is complete, unmount the SD card and insert it into the HT5 target.



## **Deploying Image onto Target** 

Power up the HT5, and login when prompted. Then execute the following to install the image from the SD card:

```
 root@ht5:# /usr/libexec/tisd/install-HT5.sh
```


when this installation completes, remove the SD card from the HT5, and power cycle the unit.


This update can be verified after unit has rebooted, by executing the commands shown below:



Check the date when kernel was rebuilt:
```
 root@ht5:# uname -a
 Linux ht5 2.6.37.6-ht5 #1 Mon Jan 21 12:25:21 UTC 2019 armv7l GNU/Linux
```

Check hanover versions file:
```
 root@ht5:# cat /etc/hanover-version
 Build Date: 2019-01-21 11:43 UTC
 Hanover Distro: 2.2.33.1-8
 Machine: dm814x-ht5
 CPU instruction set: armv7a
 Compiler: arm-arago-linux-gnueabi
 Build purpose: stib
 Build type: rel

 Versions:
 arago: 2.2.27-4-g2851156
 arago-bitbake: 2.2.0
 arago-oe-dev: 2.2.31.1
 hanover-apps: 2.2.33.1-2-g6f4a385
 hanover-system: 2.2.33-22-ga24d661
```


## **Summary of Entire Process**
Assuming that all prerequisite software has been installed, and ssh access has been granted to Bitbucket repositories:

**Create workspace**
```      
 $ sudo mkdir ~/projects
 $ sudo mkdir ~/projects/oe-stib-ht5-sdk
 $ sudo chown -R cchilumbu:cchilumbu /home/cchilumbu
```    

**Initialise workspace**
```
 $ cd ~/projects/oe-stib-ht5-sdk
 $ repo init -u git@bitbucket.org:stib-ht5/oe-env
```

**Populate workspace**
```
 $ repo sync -j4
```

**Build hanover-stib-image**
```
 $ ./all.sh -r hanover-stib-image
```

**Build release image**
```
 $ cd ~/projects/oe-stib-ht5-sdk/arago-tmp/stib/rel/deploy/eglibc/images/dm814x-ht5 
 $ ./scripts/release.sh
```
 
**Make SD card image using TIS**
```
 $ cd ~/projects/oe-stib-ht5-sdk/arago-tmp/stib/rel/deploy/eglibc/images/dm814x-ht5/release/SW-HT5-STIB/r04-rc61
 $ sudo ~/projects/oe-stib-ht5-sdk/SW-HT5-TIS/0.2.7.1/scripts/mksdimage.sh .
```

**Write SD card image to SD card**
```
 $ sudo dd if=SW-HT5-STIB-02.1900.ima of=/dev/sdc bs=1M status=progress
 $ sync
```

**Insert SD card into HT5, and install image**
```
 root@ht5:# /usr/libexec/tisd/install-HT5.sh
```

**Remove SD card from HT5, power cycle unit**



## **APPENDIX A** 

### **Validate Built Image using Simulator StibisSim** 

The following test can be performed on the target to validate the built image. It uses a proprietary in-house simulator called **StibisSim**, which is present on the target HT5. Pleaase make sure that the HT5 is connected to the TPT Panel and/or Sign for a complete system test.

The test below verifies that destination codes are interpreted correctly and displayed accordingly onto the TFT panel and/or sign.


**invoke StibisSim**
```
 root@ht5:# StibisSim localhost 10020
```

*This command should bring up a menu of options, from which (2) **Destination** should be selected.*

**Enter the destination code 0472**    
```
 Cimetiere de Bruxelles is listed as the destination on the TFT panel
```

**Enter the destination code 1201**
```
 Brussels Airpot is listed as the destination on the TFT panel
```

**Enter the destination code 1402**
```
 UZ Brussel is listed as the destination on the TFT panel
```

**Enter the destination code 1672**
```
 Berchem Station is listed as the destination on the TFT panel
```

**Enter the destination code 1701**
```
 Beaulieu is listed as the destination on the TFT panel
```



## **APPENDIX B** 

### **Helper Scripts** 

The build system is based on bitbake and includes several helper scripts in the workspace that can be used for build purposes.

To clean the build system, execute the script **clean.sh**:
```
 $ cd ~/projects/oe-stib-ht5-sdk
 $ ./clean.sh
```

To build the entire workspace in the correct order, execute **all.sh** as shown below:
```
 $ cd ~/projects/oe-stib-ht5-sdk
 $ ./all.sh -r hanover-stib-image
```

This will rebuild all the packages defined by the manifest in the correct order, retrying each package several times.

To build all or selected packages, execute the script **bb.sh** as shown below:
```
 $ cd ~/projects/oe-stib-ht5-sdk
 $ ./bb.sh -r hanover-stib-image
```


These wrapper scripts to bitbake have several options, with some of the commonly used flags shown below:
```
 $ ./bb.sh packagename option [option...]
 -b --buildfile specify bitbake recipe instead of package name
 -c --cmd       execute command e.g. clean, compile
 -F --force     force recompilation of package even if seemingly unchanged
 -k --continue  Don't abort if a target fails
 -l --retry     Retry the same target if it fails
```


To monitor the progress of bitbake, execute the script **times.sh**. This script monitors a file **times.txt** which is generated by the **bb.sh** script. It maybe convenient to link this script to the workspace base directory:
```
 $ cd ~/projects/oe-stib-ht5-sdk
 $ ln -s hanover-oe-utils/oe-env/times.sh times.sh
```


Build progress can then be monitored like this:
```
 $ cd ~/projects/oe-stib-ht5-sdk
 $ watch -n1 ./times.sh 
```

As part of the initialisation stage, some scripts are installed at the base of the workspace as symbolic links to scripts, please see snapshot of workspace below: 

```
 $ ls -al ~/projects/oe-stib-ht5-sdk
 total 92
 drwxrwxr-x 15 cchilumbu cchilumbu  4096 Jan 22 11:32 .
 drwxrwxrwx 10 cchilumbu cchilumbu  4096 Jan 22 10:56 ..
 lrwxrwxrwx  1 cchilumbu cchilumbu    34 Jan 18 12:20 all.sh -> hanover-products/stib/build_all.sh
 drwxrwxr-x  7 cchilumbu cchilumbu  4096 Jan 18 12:19 arago
 drwxrwxr-x  9 cchilumbu cchilumbu  4096 Jan 18 12:19 arago-bitbake
 drwxrwxr-x 12 cchilumbu cchilumbu  4096 Jan 18 12:19 arago-oe-dev
 lrwxrwxrwx  1 cchilumbu cchilumbu    29 Jan 18 12:19 bb.sh -> hanover-oe-utils/oe-env/bb.sh
 lrwxrwxrwx  1 cchilumbu cchilumbu    32 Jan 18 12:19 clean.sh -> hanover-oe-utils/oe-env/clean.sh
 drwxrwxr-x  2 cchilumbu cchilumbu  4096 Jan 21 14:50 conf
 drwxrwxr-x  4 cchilumbu cchilumbu  4096 Jan 22 11:31 delete-me
 drwxrwxr-x  3 cchilumbu cchilumbu  4096 Jan 21 08:21 docker
 drwxrwxr-x  4 cchilumbu cchilumbu  4096 Jan 18 13:19 .docker-run-cache
 drwxrwxr-x  2 cchilumbu cchilumbu 36864 Jan 18 12:19 downloads
 drwxrwxr-x  5 cchilumbu cchilumbu  4096 Jan 18 12:19 hanover-apps
 drwxrwxr-x  5 cchilumbu cchilumbu  4096 Jan 18 12:19 hanover-oe-utils
 drwxrwxr-x  3 cchilumbu cchilumbu  4096 Jan 18 12:19 hanover-products
 drwxrwxr-x  5 cchilumbu cchilumbu  4096 Jan 18 12:20 hanover-system
 drwxrwxr-x  7 cchilumbu cchilumbu  4096 Jan 18 12:19 .repo
 lrwxrwxrwx  1 cchilumbu cchilumbu    13 Jan 18 12:19 run.sh -> docker/run.sh
 lrwxrwxrwx  1 cchilumbu cchilumbu    32 Jan 18 12:19 times.sh -> hanover-oe-utils/oe-env/times.sh
```


The following helpers scripts, **release.sh**, **mkupdate.sh** and **mkupdate.py** are installed into the deploy area during the build and they are used for release management. A snapshot of this deploy area is shown below: 

```
 $ ls -la ~/projects/oe-stib-ht5-sdk/arago-tmp/stib/rel/deploy/eglibc/images/dm814x-ht5/scripts
 total 52
 drwxr-xr-x 2 cchilumbu domain users  4096 Jan 17 10:27 ./
 drwxr-xr-x 5 cchilumbu domain users  4096 Jan 17 11:01 ../
 -rw-r--r-- 1 cchilumbu domain users   208 Jan 17 09:25 buildenv.inc
 lrwxrwxrwx 1 cchilumbu domain users    12 Jan 17 09:25 buildenv.sh -> buildenv.inc
 -rw-r--r-- 1 cchilumbu domain users  7634 Jan 17 10:10 build_release.inc
 -rwxr-xr-x 1 cchilumbu domain users 16525 Jan 17 10:27 mkupdate.py
 -rwxr-xr-x 1 cchilumbu domain users  1416 Jan 17 10:07 mkupdate.sh
 -rw-r--r-- 1 cchilumbu domain users   940 Jan 17 10:10 release-dm814x-ht5.inc
 -rwxr-xr-x 1 cchilumbu domain users   110 Jan 17 10:07 release.sh
```


The helper script used to make the SD image, **mksdimage.sh** is part of the **TIS** release:

```
 $ ls -la ~/projects/oe-stib-ht5-sdk/SW-HT5-TIS/0.2.7.1/scripts/
 total 44
 drwxrwxr-x 2 cchilumbu cchilumbu  4096 Nov 12 16:00 .
 drwxrwxr-x 3 cchilumbu cchilumbu  4096 Nov 12 16:11 ..
 -rw-rw-r-- 1 cchilumbu cchilumbu  7634 Nov 12 16:00 build_release.inc
 -rwxrwxr-x 1 cchilumbu cchilumbu  3158 Nov 12 16:00 mksdimage.sh
 -rwxrwxr-x 1 cchilumbu cchilumbu 16525 Nov 12 16:00 mkupdate.py
 -rw-rw-r-- 1 cchilumbu cchilumbu   940 Nov 12 16:00 release-dm814x-ht5.inc
```



#### **Other useful helper scripts are described below**:

**mkupdate.sh** creates a partial binary repository to be used by opkg to update a system. It takes as first argument the release directory of the system we are updating, followed by the package manifests of the versions intended as reference. 

Assuming stib releases directory has been deployed at $HOME :

```
 $ ls -la ~/projects/releases/SW-HT5-STIB/
 total 20
 drwxr-xr-x 5 cchilumbu cchilumbu 4096 Jan 14 10:57 .
 drwxrwxr-x 3 cchilumbu cchilumbu 4096 Jan 23 15:51 ..
 drwxr-xr-x 5 cchilumbu cchilumbu 4096 May  5  2017 r04-rc41
 drwxr-xr-x 4 cchilumbu cchilumbu 4096 Jan 14 10:47 r04-rc59
 drwxr-xr-x 6 cchilumbu cchilumbu 4096 Jan 22 13:33 r04-rc61
```

An example usage of this script is shown below:

```
 $ cd ~/projects/releases/SW-HT5-STIB/r04-rc61
 $ ./scripts/mkupdate.sh opkg_status.txt ../r04-rc41 ../r04-rc59
```

The above creates two directories, each containing a tarball, that includes the packages needed from r04-rc41 to r04-rc61, and from r04-rc59 to r04-rc61. A snapshot of what is generated is shown below:

```
 $ ls -la ~/projects/releases/SW-HT5-STIB/r04-rc61 
 total 2438652
 drwxr-xr-x 7 cchilumbu cchilumbu       4096 Jan 23 15:57  .
 drwxr-xr-x 5 cchilumbu cchilumbu       4096 Jan 14 10:57  ..
 -rw-r--r-- 1 cchilumbu cchilumbu      69148 Jan 14 10:56  boot.bin
 -rw-r--r-- 1 cchilumbu cchilumbu   19660475 Jan 14 10:48  data-02.zip
 drwxrwxr-x 6 cchilumbu cchilumbu       4096 Jan 14 00:36  ipk
 -rw-r--r-- 1 cchilumbu cchilumbu     192929 Jan 14 10:48  opkg_status.txt
 drwxr-xr-x 2 cchilumbu cchilumbu       4096 Jan 22 13:41  scripts
 -rwxrwxr-x 1 cchilumbu cchilumbu     157696 Jan 14 12:03 'Software Release Report - SR1192 STIB r04-rc61.doc'
 -rw-r--r-- 1 cchilumbu cchilumbu 1992294400 Jan 16 11:08  SW-HT5-STIB-r04-rc61_TIS-0.2.7.3.ima
 -rw-r--r-- 1 cchilumbu cchilumbu  398330040 Jan 14 11:08  SW-HT5-STIB-r04-rc61_TIS-0.2.7.3.ima.xz
 -rw-r--r-- 1 cchilumbu cchilumbu   86119068 Jan 14 10:48  SW-HT5-STIB-rootfs.tar.gz
 -rw-r--r-- 1 cchilumbu cchilumbu     301795 Jan 14 10:48  SW-HT5-STIB.txt
 drwxr-xr-x 2 cchilumbu cchilumbu       4096 Jan 16 11:05  tmp
 drwxr-xr-x 4 cchilumbu cchilumbu       4096 Jan 23 15:57  update-from-r04-rc41
 drwxr-xr-x 4 cchilumbu cchilumbu       4096 Jan 23 15:57  update-from-r04-rc59
 -rw-r--r-- 1 cchilumbu cchilumbu        337 Jan 14 10:48  versions.txt
```


The directories above **update-from-r04-rc41** and **update-from-r04-rc59** are then copied to the ftp site for client.
 
**upgrade-remote.sh** is a wrapper script used to perform opkg calls on a remote system via ssh. It takes the remote target IP address (e.g. root@192.168.23.2) as first argument, optionally followed by opkg arguments. This script will connect over ssh to that target, pull the packages manifest (/usr/lib/opkg/status), generate a delta against the local build, transfer it to the /usr/local/update-src directory on the target, and then use opkg-upgrade-from.sh to upgrade the target from there.

If **upgrade-remote.sh** is used to call **opkg install** like in the following example, it will ask mkupdate.py to include the needed **.ipk** files to make it possible.


**Warning: It is recommended to build the package and the index first before running upgrade-remote.sh**:

```
 $ cd ~/projects/oe-stib-ht5-sdk
 $ ./bb.sh package-index
```

Then execute the remote upgrade script:

```
 $ cd ~/projects/oe-stib-ht5-sdk/hanover-oe-utils/oe-env
 $ ./upgrade-remote.sh root@192.168.23.2 install strace
```

The above installs package **strace** onto remote target with IP address **192.168.23.2**.


##**References:**
```
* [Docker] (https://www.docker.com)
* [Arago] (http://arago-project.org) 
* [Angstrom] (http://www.angstrom-distribution.org)
* [OpenEmbedded] (http://www.openembedded.org/wiki/Main_Page)
* [Add SSH Keys] (https://community.atlassian.com/t5/Bitbucket-questions/How-do-I-add-an-SSH-key-as-opposed-to-a-deployment-keys/qaq-p/413373)
```
